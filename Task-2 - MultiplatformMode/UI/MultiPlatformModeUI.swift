//
//  MultiPlatformModeUI.swift
//  Task-2 - MultiplatformMode
//
//  Created by Hrithik Kumar V on 22/06/22.
//

import Cocoa

//class MultiplatformModeController : MultiPlatformModeSplitViewController,MultiplatformModeControllerContract{
//    var presenter :  MultiPlatformModePresenterContract?
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        self.wantBackToolBarButton = true
//    }
//
//}


class MultiplatformModeController : ZResponsiveSplitViewController,MultiplatformModeControllerContract{
    
    var presenter :  MultiPlatformModePresenterContract?

    let toolBar : NSToolbar = {
       let toolbar = NSToolbar(identifier: .init("Default"))
        toolbar.allowsUserCustomization = true
        toolbar.displayMode = .default
       return toolbar
    }()
   
    var backArrowToolbarItem : NSToolbarItem?
    
    var wantBackToolBarButton : Bool = true
  
    private(set) var mode : Modes?{
        didSet{
            if oldValue != mode{
                applyMode()
            }
                
            
            }
        
        }
    
    
    var sideBarViewController : NSViewController?
    
    var contentListViewController : NSViewController?
    
    var detailViewController : NSViewController?
    
    var sidebarSplitItem : ZResponsiveSplitItem?
    
    var contentSplitViewController : NSSplitViewController = {
        let contentSplitViewController = NSSplitViewController()
        return contentSplitViewController
    }()

    lazy var contentSplitViewControllerSplitItem : ZResponsiveSplitItem = {
        let contentSplitViewController = contentSplitViewController
        let contentSplitViewControllerSplitItem = ZResponsiveSplitItem(viewController: contentSplitViewController)
        contentSplitViewControllerSplitItem.itemIdentifier = "Content Container"
        return contentSplitViewControllerSplitItem
    }()
        
    var contentListSplitViewItem : ZResponsiveSplitItem?
        
    var detailViewSplitItem : ZResponsiveSplitItem?
  
    
    override func viewDidLoad() {
        splitView.dividerStyle = .thin
        modeSetCallback = { [weak self] (_ modeDetails: (modeName: String, minimumWidth: CGFloat)) in
            self?.mode = Modes(rawValue: modeDetails.modeName)
        }
        self.viewModes = [(modeName: "Mobile", minimumWidth: 400),(modeName: "Tab", minimumWidth: 700),(modeName: "Desktop", minimumWidth: 1000)]
        super.viewDidLoad()
    }
    
    override func viewDidAppear() {
        if(wantBackToolBarButton){
            toolBar.delegate = self
            self.view.window?.toolbar = toolBar
        }
    }
        
    
    
    func addSideBarViewController(viewController : NSViewController){
        if(sidebarSplitItem == nil){
            sideBarViewController = viewController
            sidebarSplitItem = ZResponsiveSplitItem(sidebarWithViewController: viewController)
            sidebarSplitItem?.canCollapse = false
            sidebarSplitItem?.isSideBarItem = true
            sidebarSplitItem?.itemIdentifier = "SideBar"
            sidebarSplitItem?.viewModes = [(modeName: "Mobile", minimumWidth: 300, maximumWidth: 1000),(modeName: "Tab", minimumWidth: 300, maximumWidth: 1000),(modeName: "Desktop", minimumWidth: 300, maximumWidth: 1000)]
            self.addSplitViewItem(sidebarSplitItem!)
          
        }
    }
    
    func addContentListViewController(viewController : NSViewController){
        if(contentListSplitViewItem == nil){
            
            addSplitViewItem(contentSplitViewControllerSplitItem)
            contentListViewController = viewController
            contentListSplitViewItem = ZResponsiveSplitItem(contentListWithViewController: viewController)
            contentListSplitViewItem?.viewModes = [(modeName: "Mobile", minimumWidth: 300, maximumWidth: 1000),(modeName: "Tab", minimumWidth: 300, maximumWidth: 1000),(modeName: "Desktop", minimumWidth: 300, maximumWidth: 1000)]
            contentListSplitViewItem?.itemIdentifier = "Content"
            contentSplitViewController.addSplitViewItem(contentListSplitViewItem!)
            applyMode()
        }
       
    }
    
    func addDetailViewController(viewController : NSViewController){
        if(detailViewSplitItem == nil){
            detailViewController = viewController
            detailViewSplitItem = ZResponsiveSplitItem(viewController: viewController)
            detailViewSplitItem?.viewModes = [(modeName: "Mobile", minimumWidth: 300, maximumWidth: 1000),(modeName: "Tab", minimumWidth: 300, maximumWidth: 1000),(modeName: "Desktop", minimumWidth: 400, maximumWidth: 1000)]
            detailViewSplitItem?.itemIdentifier = "Detail"
            contentSplitViewController.addSplitViewItem(detailViewSplitItem!)
            applyMode()
        }
       
    }
    
    func didTapBackButton(){
                
        if(detailViewController != nil){
                contentListSplitViewItem?.isCollapsed = false
                removeDetailSplitViewItem()
                    
        }
        else if(contentListViewController != nil){
                sidebarSplitItem?.isCollapsed = false
                removeContentListSplitViewItem()
        }
                
    }
    
    private func applyMode(){
        switch mode{
            case .Mobile:
                mobileMode()
          
                break
            case .Tab:
                tabMode()
                break
            case .Desktop:
                desktopMode()
                break
        
            case .none:
                break
            }
    }
        
       
    private func desktopMode(){
        self.view.window?.title = "Desktop Mode"
        
        if(sideBarViewController != nil){
            sidebarSplitItem?.isCollapsed = false
            
        }
        if(contentListViewController != nil){
            contentSplitViewControllerSplitItem.isCollapsed  = false
            contentListSplitViewItem?.isCollapsed = false
            
           
        }
        if(detailViewController != nil){
            contentSplitViewController.splitView.isVertical = true
            contentSplitViewControllerSplitItem.isCollapsed  = false
            detailViewSplitItem?.isCollapsed = false
            contentSplitViewController.splitView.setPosition(splitView.frame.width/2, ofDividerAt: 0)
        }
        
    }
    
    private func tabMode(){
        self.view.window?.title = "Tab Mode"
        if(sideBarViewController != nil){
            sidebarSplitItem?.isCollapsed = false
        }
        if(detailViewController != nil && contentListViewController != nil){
            contentSplitViewController.splitView.isVertical = false
            contentSplitViewControllerSplitItem.isCollapsed  = false
            detailViewSplitItem?.isCollapsed = false
            contentListSplitViewItem?.isCollapsed = false
            contentSplitViewController.splitView.setPosition(splitView.frame.height/2, ofDividerAt: 0)
            self.splitView.setPosition(300, ofDividerAt: 0)
            
        }
        else if(contentListViewController != nil){
            contentSplitViewControllerSplitItem.isCollapsed  = false
            contentListSplitViewItem?.isCollapsed = false
        }
        
    }
    
    private func mobileMode(){
        self.view.window?.title = "Mobile Mode"
        if(detailViewController != nil){
            contentSplitViewController.splitView.isVertical = true
            contentSplitViewControllerSplitItem.isCollapsed = false
            detailViewSplitItem?.isCollapsed = false
            contentListSplitViewItem?.isCollapsed = true
            sidebarSplitItem?.isCollapsed = true
            
        }
        else if(contentListViewController != nil){
            contentSplitViewControllerSplitItem.isCollapsed  = false
            detailViewSplitItem?.isCollapsed = true
            contentListSplitViewItem?.isCollapsed = false
            sidebarSplitItem?.isCollapsed = true
            
            
        }
        else if(sideBarViewController != nil){
            contentSplitViewControllerSplitItem.isCollapsed = true
            detailViewSplitItem?.isCollapsed = true
            contentListSplitViewItem?.isCollapsed = true
            sidebarSplitItem?.isCollapsed = false

        }
        
    }

    
    private func removeSideBarSplitViewItem(){
        removeSplitViewItem(sidebarSplitItem!)
        sidebarSplitItem = nil
        sideBarViewController = nil
        applyMode()

    }
    
    private func removeContentListSplitViewItem(){
        contentSplitViewController.removeSplitViewItem(contentListSplitViewItem!)
        removeSplitViewItem(contentSplitViewControllerSplitItem)
        contentListSplitViewItem = nil
        contentListViewController = nil
        applyMode()
        
    }
    
    private func removeDetailSplitViewItem(){
        contentSplitViewController.removeSplitViewItem(detailViewSplitItem!)
        detailViewSplitItem = nil
        detailViewController = nil
        applyMode()
    }
    
    

    
    
    enum Modes : String{
        case Mobile = "Mobile"
        case Tab = "Tab"
        case Desktop = "Desktop"
    }
    
    deinit{
        self.view.window?.toolbar = nil
    }
    
  
}



extension MultiplatformModeController : NSToolbarDelegate {
    
    
    
    func toolbar(_ toolbar: NSToolbar, itemForItemIdentifier itemIdentifier: NSToolbarItem.Identifier, willBeInsertedIntoToolbar flag: Bool) -> NSToolbarItem? {
        
        if itemIdentifier == NSToolbarItem.Identifier.backwardArrow {
            backArrowToolbarItem = customToolbarItem(itemForItemIdentifier: NSToolbarItem.Identifier.backwardArrow.rawValue,
                                                     label: NSLocalizedString("Back", comment: ""),
                                                     paletteLabel: NSLocalizedString("Back", comment: ""),
                                                     toolTip: NSLocalizedString("Go To Previous Page", comment: ""),
                                                     iconImage: NSImage(systemSymbolName: "arrow.backward", accessibilityDescription: "Backward  Arrow")!)!
            return backArrowToolbarItem!
                
        }
        else {
            return nil
        }
    }
    func toolbarAllowedItemIdentifiers(_ toolbar: NSToolbar) -> [NSToolbarItem.Identifier] {
        return [.backwardArrow]
    }
    func toolbarDefaultItemIdentifiers(_ toolbar: NSToolbar) -> [NSToolbarItem.Identifier] {
        return [.backwardArrow]
    }
    
    func toolbarWillAddItem(_ notification: Notification) {
        
    }
    
    func toolbarDidRemoveItem(_ notification: Notification) {
        
    }
    
    

    func customToolbarItem(
        itemForItemIdentifier itemIdentifier: String,
        label: String,
        paletteLabel: String,
        toolTip: String,
        iconImage: NSImage ) -> NSToolbarItem? {
        
        let toolbarItem = NSToolbarItem(itemIdentifier: NSToolbarItem.Identifier(rawValue: itemIdentifier))
        toolbarItem.label = label
        toolbarItem.paletteLabel = paletteLabel
        toolbarItem.toolTip = toolTip
        toolbarItem.target = self
        toolbarItem.image = iconImage
        toolbarItem.action = #selector(didTapToolBarItem(sender:))
        toolbarItem.isNavigational = true
        toolbarItem.isBordered = true
        
        let menuItem: NSMenuItem = NSMenuItem()
        menuItem.submenu = nil
        menuItem.title = label
        menuItem.target = self
        menuItem.action = #selector(didTapMenuItem(sender:))
        toolbarItem.menuFormRepresentation = menuItem
        
        return toolbarItem
    }
    
    @objc func didTapToolBarItem(sender : NSToolbarItem){
        self.didTapBackButton()
    }
    
    @objc func didTapMenuItem(sender : NSMenuItem){
        self.didTapBackButton()
    }
}



    


protocol MultiplatformModeControllerContract : NSSplitViewController{
    var presenter :  MultiPlatformModePresenterContract? { get set }
    
    func addSideBarViewController(viewController : NSViewController)
    
    
    func addContentListViewController(viewController : NSViewController)
    
    
    func addDetailViewController(viewController : NSViewController)
    
    func didTapBackButton()
}
