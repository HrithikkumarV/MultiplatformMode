//
//  SearchTablePresenter.swift
//  Task-2 - MultiplatformMode
//
//  Created by Hrithik Kumar V on 22/06/22.
//

import Cocoa

class SearchTablePresenter : SearchTablePresenterContract{
    
    weak var router : SearchTableRouterContract?
    weak var viewController : SearchTableViewControllerContract?
    let getPincodeDetails: GetPincodeDetails
    
    init(getPincodeDetails : GetPincodeDetails){
        self.getPincodeDetails = getPincodeDetails
    }
    func getPicodeDetails(pincode : String){
        if(pincode.count == 6){
           
            getPincodeDetails.execute(request: GetPincodeDetailsRequest(pincode: pincode, requestType: .server)) { response  in
                if(response.responseStatus == .success){
                    self.viewController?.updatePincodeDetailsInSearchBar(PincodeDetails: response.pincodeDetails)
                }
                else{
                    print(response.responseStatus)
                }
                }
            }
        }
    
    
    
    
        
   
    
    func didTapClearHistory() {
        router?.addDisplayRestaurants()
    }
   
    

}

protocol SearchTablePresenterContract : AnyObject{
    var router : SearchTableRouterContract? {get set}
    var viewController : SearchTableViewControllerContract? {get set}
    func didTapClearHistory()
    func getPicodeDetails(pincode : String)
}
