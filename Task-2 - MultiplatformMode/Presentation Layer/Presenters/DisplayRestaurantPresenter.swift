//
//  DisplayRestaurantPresenter.swift
//  Task-2 - MultiplatformMode
//
//  Created by Hrithik Kumar V on 22/06/22.
//

import Cocoa

class DisplayRestaurantsPresenter : DisplayRestaurantsPresenterContract{
    
    weak var router : DisplayRestaurantsRouterContract?
    weak var viewController : DisplayRestaurantsCollectionViewControllerContract?
    let getListOfRestaurantDetails : GetListOfRestaurantDetails
    
    init(getListOfRestaurantDetails : GetListOfRestaurantDetails){
        self.getListOfRestaurantDetails = getListOfRestaurantDetails
    }
    
    func getListOfRestaurantDetails(locality : String,pincode : String){
        getListOfRestaurantDetails.execute(request: getRestaurantsDetailsRequest(locality: locality, pincode: pincode, requestType: .local)) { response in
            if(response.responseStatus == .success){
                self.viewController?.updateListOfRestaurantDetailsInView(listOfRestaurantDetails: response.listOfRestaurantDetails)
            }
            else{
                print(response.responseStatus)
            }
        }
    }
        
    
   
    
    
}

protocol DisplayRestaurantsPresenterContract : AnyObject{
    var router : DisplayRestaurantsRouterContract? {get set}
    var viewController : DisplayRestaurantsCollectionViewControllerContract? {get set}
    func getListOfRestaurantDetails(locality : String,pincode : String)
}
