//
//  MultiPlatformModePresenter.swift
//  Task-2 - MultiplatformMode
//
//  Created by Hrithik Kumar V on 22/06/22.
//

import Cocoa

class MultiPlatformModePresenter : MultiPlatformModePresenterContract{
    
    weak var router : MultiPlatformModeRouterContract?
    weak var viewController : MultiplatformModeControllerContract?
    
   
    func didTapBackButton(){
        self.viewController?.didTapBackButton()
    }
    
    func addSideBarViewController(viewController : NSViewController){
        self.viewController?.addSideBarViewController(viewController: viewController)
        
    }
    
    func addContentListViewController(viewController : NSViewController){
        
        self.viewController?.addContentListViewController(viewController: viewController)
    }
    
    func addDetailViewController(viewController : NSViewController){
        self.viewController?.addDetailViewController(viewController: viewController)
    }
}

protocol MultiPlatformModePresenterContract : AnyObject{
    
    var router : MultiPlatformModeRouterContract? {get set}
    
    var viewController : MultiplatformModeControllerContract? {get set}
    
    func addSideBarViewController(viewController : NSViewController)
    
    
    func addContentListViewController(viewController : NSViewController)
    
    
    func addDetailViewController(viewController : NSViewController)
    
    func didTapBackButton()
    
}
