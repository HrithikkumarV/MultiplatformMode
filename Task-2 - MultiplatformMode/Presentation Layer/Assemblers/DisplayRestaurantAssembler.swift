//
//  DisplayRestaurantAssembler.swift
//  Task-2 - MultiplatformMode
//
//  Created by Hrithik Kumar V on 22/06/22.
//

import Foundation
class DisplayRestaurantsAssembler{
    
    static func getBaseViewController(router : DisplayRestaurantsRouterContract) -> DisplayRestaurantsCollectionViewController{
       
        let dataManager : DataManagerContracts = DataManager(databaseService: InjectService.databaseService ,networkService: InjectService.networkService)
        let useCase = GetListOfRestaurantDetails(dataManager: dataManager)
        let presenter = DisplayRestaurantsPresenter(getListOfRestaurantDetails: useCase)
        let viewController = DisplayRestaurantsCollectionViewController()
        presenter.router = router
        presenter.viewController = viewController
        viewController.presenter = presenter
        return viewController
    }
    
}
