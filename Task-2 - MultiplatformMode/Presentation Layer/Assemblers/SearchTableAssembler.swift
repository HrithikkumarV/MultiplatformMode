//
//  SearchTableAssembler.swift
//  Task-2 - MultiplatformMode
//
//  Created by Hrithik Kumar V on 22/06/22.
//

import Foundation
class SearchTableAssembler{
    static func getBaseViewController(router : SearchTableRouterContract) -> SearchTableViewController{
        let dataManager : DataManagerContracts = DataManager(databaseService: InjectService.databaseService ,networkService: InjectService.networkService)
        let useCase : GetPincodeDetails = GetPincodeDetails(dataManager: dataManager)
        let viewController = SearchTableViewController()
        let presenter = SearchTablePresenter(getPincodeDetails: useCase)
        presenter.router = router
        presenter.viewController = viewController
        viewController.presenter = presenter
        return viewController
    }
    
}
