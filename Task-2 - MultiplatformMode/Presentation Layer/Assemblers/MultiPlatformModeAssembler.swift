//
//  MultiPlatformModeAssembler.swift
//  Task-2 - MultiplatformMode
//
//  Created by Hrithik Kumar V on 22/06/22.
//

import Cocoa

class MultiPlatformModeAssembler{
    static func getBaseViewController(router : MultiPlatformModeRouterContract) -> MultiplatformModeController{
        let viewController = MultiplatformModeController()
        let presenter = MultiPlatformModePresenter()
        presenter.router = router
        presenter.viewController = viewController
        viewController.presenter = presenter
        return viewController
    }
    
}
