//
//  DisplayRestaurantRouterContract.swift
//  Task-2 - MultiplatformMode
//
//  Created by Hrithik Kumar V on 22/06/22.
//

import Foundation
protocol DisplayRestaurantsRouterContract : AnyObject{
    var displayRestaurantsbaseViewController  : DisplayRestaurantsCollectionViewControllerContract? {get set}
    
}
