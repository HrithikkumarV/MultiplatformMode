//
//  MainScreenRouterContract.swift
//  Task-2 - MultiplatformMode
//
//  Created by Hrithik Kumar V on 22/06/22.
//

import Foundation

protocol MainScreenRouterContract : AnyObject{
    var mainScreenBaseViewController  : MainViewControllerContract? {get set}
    func addSearchTable()
    
}
