//
//  searchTableRouterContract.swift
//  Task-2 - MultiplatformMode
//
//  Created by Hrithik Kumar V on 22/06/22.
//

import Foundation

protocol SearchTableRouterContract : AnyObject{
    var searchTableBaseViewController  : SearchTableViewControllerContract? {get set}
   
    func addDisplayRestaurants()
}
