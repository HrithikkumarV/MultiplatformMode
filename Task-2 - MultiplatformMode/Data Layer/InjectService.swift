//
//  InjectDataBaseInstance.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 27/10/21.
//

import Foundation

class InjectService{
    static let databaseService: DatabaseServiceContract  = DatabaseService()
    static let networkService : NetworkServiceContract = NetworkService()
    
    private init(){
    }
    
    
}
