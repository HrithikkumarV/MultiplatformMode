//
//  MenuDao.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 28/01/22.
//

import Foundation
import SQLite3

extension DatabaseService : RestaurantMenuDetailsDAOContract , UserDisplayMenuDetailsPageDAOContract{
    
   
    
    
    func persistMenuCategory(restaurantId : Int , categoryType : String) throws -> Bool{
        
        var isExecuted = false
        let insertQuery = "INSERT INTO menu_category(restaurant_id, category_name) VALUES (?, ?)"
        let insertQueryStatement : OpaquePointer? = try prepareStatement(query: insertQuery)
        guard sqlite3_bind_int(insertQueryStatement , 1, Int32( restaurantId)) == SQLITE_OK &&
                sqlite3_bind_text(insertQueryStatement , 2, (categoryType as NSString).utf8String, -1, nil) == SQLITE_OK
        else {
            throw SQLiteError.Bind(message: errorMessage)
        }
        if sqlite3_step(insertQueryStatement) == SQLITE_DONE {
            isExecuted = true
        } else{
                throw SQLiteError.Step(message: errorMessage)
            }
        
            sqlite3_finalize(insertQueryStatement)
        
        return isExecuted
        
    }
    
    func getCategoryDetails(restaurantId : Int) throws -> [(categoryId : Int , categoryName : String)] {
        
        var categoryId : Int = 0
        var categoryName : String = ""
        var category : [(categoryId : Int , categoryName : String)] = []
        let query = "SELECT category_id , category_name  FROM menu_category where restaurant_id = ? and category_status = 1;"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)

        guard sqlite3_bind_int(queryStatement , 1, Int32(restaurantId)) == SQLITE_OK
        else {
            throw SQLiteError.Bind(message: errorMessage)
        }

            while(sqlite3_step(queryStatement) == SQLITE_ROW){
                categoryId = Int( sqlite3_column_int(queryStatement, 0))
                categoryName = String(cString: sqlite3_column_text(queryStatement, 1))
                category.append((categoryId : categoryId , categoryName : categoryName))
            }

        
            sqlite3_finalize(queryStatement)
        
         return category
    }
    
    func getCategoryId(categoryName : String , restaurantId : Int) throws -> Int{
        
        var categoryId : Int = 0
        let query = "SELECT category_id FROM menu_category where restaurant_id  = ? and  category_name  = ? ;"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)

        guard sqlite3_bind_int(queryStatement , 1, Int32(restaurantId)) == SQLITE_OK &&
                sqlite3_bind_text(queryStatement , 2, (categoryName as NSString).utf8String, -1, nil) == SQLITE_OK
        else {
            throw SQLiteError.Bind(message: errorMessage)
        }

            if(sqlite3_step(queryStatement) == SQLITE_ROW){
                categoryId = Int( sqlite3_column_int(queryStatement, 0))
            }

        
            sqlite3_finalize(queryStatement)
        
         return categoryId
    }
    
    func removeCategory(categoryId: Int) throws -> Bool {
        
        var isExecuted = false
        let Query = "UPDATE menu_category SET category_status = 0 where category_id = ?; "
        let QueryStatement : OpaquePointer? = try prepareStatement(query: Query)
        guard sqlite3_bind_int(QueryStatement , 1, Int32(categoryId)) == SQLITE_OK
            else {
            throw SQLiteError.Bind(message: errorMessage)
            }
        
        if sqlite3_step(QueryStatement) == SQLITE_DONE {
            isExecuted = true
        } else{
                throw SQLiteError.Step(message: errorMessage)
            }
        
            sqlite3_finalize(QueryStatement)
        
        return isExecuted
    }

    
    func removeMenuOfCategory(categoryId: Int) throws -> Bool {
        
        var isExecuted = false
        let Query = " UPDATE menu_details SET menu_isdeleted = 1 , menu_status = 0 , menu_isavailable = 0 where category_id = ?; "
        let QueryStatement : OpaquePointer? = try prepareStatement(query: Query)
        guard sqlite3_bind_int(QueryStatement , 1, Int32(categoryId)) == SQLITE_OK
            else {
            throw SQLiteError.Bind(message: errorMessage)
            }
        
        if sqlite3_step(QueryStatement) == SQLITE_DONE {
            isExecuted = true
        } else{
                throw SQLiteError.Step(message: errorMessage)
            }
        
            sqlite3_finalize(QueryStatement)
        
        return isExecuted
    }
    func UpdateCategoryName(CategoryName : String , categoryId : Int) throws -> Bool{
        
        var categoryNameUpdated = false
        let query = "UPDATE menu_category SET category_name = ? where category_id = ? ;"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
        
        guard sqlite3_bind_text(queryStatement , 1, (CategoryName as NSString).utf8String, -1, nil) == SQLITE_OK &&
            sqlite3_bind_int(queryStatement, 2, Int32(categoryId)) == SQLITE_OK
        else {
            throw SQLiteError.Bind(message: errorMessage)
        }
        
        if(sqlite3_step(queryStatement) == SQLITE_DONE){
            categoryNameUpdated = true
        }
        sqlite3_finalize(queryStatement)
        
        return categoryNameUpdated
    }
    

    
    
    
    
    
    
    func persistMenuDetails(menuDetails: MenuDetails) throws -> Bool{
        ()
        var isExecuted = false
        let insertQuery = "INSERT INTO menu_details(restaurant_id, menu_name ,menu_description , menu_price , category_id , menu_tarian_type , menu_image ,menu_created_at ) VALUES ( ?, ?, ?, ?, ?, ?,?, datetime('now','localtime'))"
        let insertQueryStatement : OpaquePointer? = try prepareStatement(query: insertQuery)
        guard sqlite3_bind_int(insertQueryStatement , 1, Int32( menuDetails.restaurantId)) == SQLITE_OK &&
                sqlite3_bind_text(insertQueryStatement , 2, (menuDetails.menuName as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(insertQueryStatement , 3, (menuDetails.menuDescription as NSString).utf8String, -1, nil) == SQLITE_OK &&
        sqlite3_bind_int(insertQueryStatement , 4, Int32(menuDetails.menuPrice)) == SQLITE_OK &&
        sqlite3_bind_int(insertQueryStatement , 5, Int32(menuDetails.menuCategoryId)) == SQLITE_OK &&
        sqlite3_bind_text(insertQueryStatement , 6, (menuDetails.menuTarianType as NSString).utf8String, -1, nil) == SQLITE_OK
        else {
            throw SQLiteError.Bind(message: errorMessage)
        }
        
        if(menuDetails.menuImage != nil) {
            guard menuDetails.menuImage?.withUnsafeBytes({ buffer -> Int32 in
        sqlite3_bind_blob(insertQueryStatement, 7,buffer.baseAddress, Int32(menuDetails.menuImage!.count), nil)
            }) == SQLITE_OK
            else {
                throw SQLiteError.Bind(message: errorMessage)
            }
        }
        if sqlite3_step(insertQueryStatement) == SQLITE_DONE {
            isExecuted = true
        } else{
                throw SQLiteError.Step(message: errorMessage)
            }
            sqlite3_finalize(insertQueryStatement)
        ()
        return isExecuted
        
    }
    
    func getMenuDetailsofRestaurantForUser(restaurantId: Int) throws -> [MenuContentDetails]{
        ()
        var menuDetailsOfRestaurant : [MenuContentDetails] = []
        
        let query = "SELECT  menu_name ,menu_description , menu_price , category_id , menu_tarian_type , menu_image , menu_id, menu_isavailable,menu_next_available_at, menu_status FROM  menu_details WHERE restaurant_id = ? and menu_status = 1;"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
        
        guard sqlite3_bind_int(queryStatement , 1, Int32(restaurantId)) == SQLITE_OK
            else {
            throw SQLiteError.Bind(message: errorMessage)
            }
        while sqlite3_step(queryStatement) == SQLITE_ROW{
            var menuContentDetails = MenuContentDetails()
            var menuDetails = MenuDetails()
            menuDetails.menuName = String(cString: sqlite3_column_text(queryStatement, 0))
            menuDetails.menuDescription = String(cString: sqlite3_column_text(queryStatement, 1))
            menuDetails.menuPrice =  Int(sqlite3_column_int(queryStatement, 2))
            menuDetails.menuCategoryId =  Int(sqlite3_column_int(queryStatement, 3))
            menuDetails.menuTarianType =  String(cString: sqlite3_column_text(queryStatement, 4))
            let length : Int? = Int(sqlite3_column_bytes(queryStatement, 5))
            let menuImage  : NSData? = NSData(bytes: sqlite3_column_blob(queryStatement, 5), length: length ?? 0)
            if  length! != 0 {
                menuDetails.menuImage =  menuImage! as Data
            }
            menuContentDetails.menuDetails =  menuDetails
            menuContentDetails.menuId = Int(sqlite3_column_int(queryStatement, 6))
            menuContentDetails.menuIsAvailable =  Int(sqlite3_column_int(queryStatement, 7))
            menuContentDetails.menuNextAvailableAt =  String(cString: sqlite3_column_text(queryStatement, 8))
            menuContentDetails.menuStatus =  Int(sqlite3_column_int(queryStatement, 9))
            menuDetailsOfRestaurant.append(menuContentDetails)
        }
        
            sqlite3_finalize(queryStatement)
        ()
        return menuDetailsOfRestaurant
    }
    
    func getMenuDetailsofRestaurant(restaurantId: Int) throws -> [MenuContentDetails]{
        ()
        var menuDetailsOfRestaurant : [MenuContentDetails] = []
        
        let query = "SELECT  menu_name ,menu_description , menu_price , category_id , menu_tarian_type , menu_image , menu_id, menu_isavailable,menu_next_available_at, menu_status FROM  menu_details WHERE restaurant_id = ? and menu_isdeleted = 0;"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
        
        guard sqlite3_bind_int(queryStatement , 1, Int32(restaurantId)) == SQLITE_OK
            else {
            throw SQLiteError.Bind(message: errorMessage)
            }
        while sqlite3_step(queryStatement) == SQLITE_ROW{
            var menuContentDetails = MenuContentDetails()
            var menuDetails = MenuDetails()
            menuDetails.menuName = String(cString: sqlite3_column_text(queryStatement, 0))
            menuDetails.menuDescription = String(cString: sqlite3_column_text(queryStatement, 1))
            menuDetails.menuPrice =  Int(sqlite3_column_int(queryStatement, 2))
            menuDetails.menuCategoryId =  Int(sqlite3_column_int(queryStatement, 3))
            menuDetails.menuTarianType =  String(cString: sqlite3_column_text(queryStatement, 4))
            let length : Int? = Int(sqlite3_column_bytes(queryStatement, 5))
            let menuImage  : NSData? = NSData(bytes: sqlite3_column_blob(queryStatement, 5), length: length ?? 0)
            if  length! != 0 {
                menuDetails.menuImage =  menuImage! as Data
            }
            menuContentDetails.menuDetails =  menuDetails
            menuContentDetails.menuId =  Int(sqlite3_column_int(queryStatement, 6))
            menuContentDetails.menuIsAvailable =  Int(sqlite3_column_int(queryStatement, 7))
            menuContentDetails.menuNextAvailableAt =  String(cString: sqlite3_column_text(queryStatement, 8))
            menuContentDetails.menuStatus =  Int(sqlite3_column_int(queryStatement, 9))
            menuDetailsOfRestaurant.append(menuContentDetails)
        }
        
            sqlite3_finalize(queryStatement)
        ()
        return menuDetailsOfRestaurant
    }
    
    func removeMenu(menuId: Int) throws -> Bool {
        ()
        var isExecuted = false
        let Query = "UPDATE menu_details  SET menu_isdeleted = 1 , menu_status = 0 , menu_isavailable = 0 where menu_id = ? ;"
        
        let QueryStatement : OpaquePointer? = try prepareStatement(query: Query)
        guard sqlite3_bind_int(QueryStatement , 1, Int32(menuId)) == SQLITE_OK
            else {
            throw SQLiteError.Bind(message: errorMessage)
            }
        
        if sqlite3_step(QueryStatement) == SQLITE_DONE {
            isExecuted  = true
        } else{
                throw SQLiteError.Step(message: errorMessage)
            }
        
            sqlite3_finalize(QueryStatement)
        ()
        return isExecuted
    }
    
    func updateMenuDetails(menuContentDetails: MenuContentDetails) throws -> Bool {
        ()
        var isExecuted = false
        let updateQuery = "UPDATE  menu_details  SET menu_name = ?, menu_description = ?, menu_price = ?, category_id = ? , menu_tarian_type = ? , menu_image = ?, menu_isavailable = ?, menu_next_available_at = ? ,  menu_status = ?  WHERE menu_id = ?"
        let updateQueryStatement : OpaquePointer? = try prepareStatement(query: updateQuery)
        let menuDetails = menuContentDetails.menuDetails
        guard
        sqlite3_bind_text(updateQueryStatement , 1, (menuDetails.menuName as NSString).utf8String, -1, nil) == SQLITE_OK &&
        sqlite3_bind_text(updateQueryStatement , 2, (menuDetails.menuDescription as NSString).utf8String, -1, nil) == SQLITE_OK &&
        sqlite3_bind_int(updateQueryStatement , 3, Int32(menuDetails.menuPrice)) == SQLITE_OK &&
        sqlite3_bind_int(updateQueryStatement , 4, Int32(menuDetails.menuCategoryId)) == SQLITE_OK &&
        sqlite3_bind_text(updateQueryStatement , 5, (menuDetails.menuTarianType as NSString).utf8String, -1, nil) == SQLITE_OK &&
            sqlite3_bind_int(updateQueryStatement , 7, Int32( menuContentDetails.menuIsAvailable)) == SQLITE_OK &&
            sqlite3_bind_int(updateQueryStatement , 9, Int32( menuContentDetails.menuStatus)) == SQLITE_OK &&
            sqlite3_bind_text(updateQueryStatement , 8, (menuContentDetails.menuNextAvailableAt as NSString).utf8String, -1, nil) == SQLITE_OK &&
            sqlite3_bind_int(updateQueryStatement , 10, Int32( menuContentDetails.menuId)) == SQLITE_OK
        else {
            throw SQLiteError.Bind(message: errorMessage)
        }
        
        if(menuDetails.menuImage != nil) {
            guard menuDetails.menuImage?.withUnsafeBytes({ buffer -> Int32 in
        sqlite3_bind_blob(updateQueryStatement, 6,buffer.baseAddress, Int32(menuDetails.menuImage!.count), nil)
            }) == SQLITE_OK
            else {
                throw SQLiteError.Bind(message: errorMessage)
            }
        }
        if sqlite3_step(updateQueryStatement) == SQLITE_DONE {
            isExecuted = true
        } else{
                throw SQLiteError.Step(message: errorMessage)
            }
            sqlite3_finalize(updateQueryStatement)
        ()
        return isExecuted
    }
    
    
}
