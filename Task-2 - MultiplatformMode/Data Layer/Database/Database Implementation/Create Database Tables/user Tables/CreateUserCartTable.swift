//
//  CreateUserCartTable.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 07/03/22.
//

import Foundation
import SQLite3
extension DatabaseService{
    
    func initialiseCreateUserCartTable() throws{
        try createUserCartTable()
    }
    
    private func createUserCartTable() throws{
        let query = "CREATE TABLE IF NOT EXISTS user_cart(cart_item_id INTEGER DEFAULT 1 PRIMARY KEY AUTOINCREMENT , menu_id INTEGER NOT NULL , restaurant_id INTEGER NOT NULL,quantity INTEGER NOT NULL,FOREIGN KEY (restaurant_id) REFERENCES restaurant_accounts (restaurant_id) ON UPDATE CASCADE ON DELETE CASCADE,FOREIGN KEY (menu_id) REFERENCES menu_details (menu_id) ON UPDATE CASCADE ON DELETE CASCADE);"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
            guard sqlite3_step(queryStatement) == SQLITE_DONE else{
                throw SQLiteError.Step(message: errorMessage)
            }
            sqlite3_finalize(queryStatement)
    }
}
