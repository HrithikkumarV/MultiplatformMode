//
//  CreateSearchHistoryTable.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 02/04/22.
//

import Foundation
import SQLite3

extension DatabaseService {
    
    func initialiseCreateSearchHistoryTable() throws{
        try createSearchHistoryTable()
    }
    
    private func createSearchHistoryTable() throws{
        let query = "CREATE TABLE IF NOT EXISTS search_history(search_history_id INTEGER DEFAULT 1 PRIMARY KEY AUTOINCREMENT , search_item_name TEXT NOT NULL,search_item_type TEXT);"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
            guard sqlite3_step(queryStatement) == SQLITE_DONE else{
                throw SQLiteError.Step(message: errorMessage)
            }
            sqlite3_finalize(queryStatement)
        print("Created search history")
    }
}

