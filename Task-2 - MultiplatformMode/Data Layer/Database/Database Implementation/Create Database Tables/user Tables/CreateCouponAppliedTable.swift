//
//  CreateCouponAppliedTable.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 14/03/22.
//

import Foundation
import SQLite3

extension DatabaseService {
    
    func initialiseCreateCouponAppliedTable() throws{
        try createCouponAppliedTable()
    }
    
    private func createCouponAppliedTable() throws{
        let query = "CREATE TABLE IF NOT EXISTS coupon_applied(coupon_applied_id INTEGER DEFAULT 1 PRIMARY KEY AUTOINCREMENT , coupon_code TEXT NOT NULL);"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
            guard sqlite3_step(queryStatement) == SQLITE_DONE else{
                throw SQLiteError.Step(message: errorMessage)
            }
            sqlite3_finalize(queryStatement)
    }
}
