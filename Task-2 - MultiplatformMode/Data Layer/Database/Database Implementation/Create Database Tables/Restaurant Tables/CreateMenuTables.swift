//
//  CreateMenuTables.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 02/03/22.
//

import Foundation
import SQLite3

extension DatabaseService {
    func initialiseCreateMenuTables() throws{
        try createMenuCategoryTable()
        try createMenuDetailsTable()
        
    }
    
    private func createMenuCategoryTable() throws{
        let query = "CREATE TABLE IF NOT EXISTS menu_category(category_id INTEGER DEFAULT 0 PRIMARY KEY AUTOINCREMENT, restaurant_id INTEGER NOT NULL, category_name TEXT NOT NULL,category_status INTEGER DEFAULT 1 NOT NULL,FOREIGN KEY (restaurant_id) REFERENCES restaurant_accounts (restaurant_id) ON UPDATE CASCADE ON DELETE CASCADE);"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
            guard sqlite3_step(queryStatement) == SQLITE_DONE else{
                throw SQLiteError.Step(message: errorMessage)
            }
            sqlite3_finalize(queryStatement)
    
    }
    
    private func createMenuDetailsTable() throws {
        
       let query = "CREATE TABLE IF NOT EXISTS menu_details(menu_id INTEGER DEFAULT 0 PRIMARY KEY AUTOINCREMENT, restaurant_id INTEGER NOT NULL, menu_name TEXT NOT NULL ,menu_description TEXT NOT NULL, menu_price INTEGER DEFAULT 0 NOT NULL, category_id INTEGER NOT NULL, menu_tarian_type TEXT NOT NULL , menu_image BLOB, menu_isavailable INTEGER DEFAULT 1 NOT NULL, menu_next_available_at TEXT DEFAULT '' NOT NULL ,menu_created_at TEXT NOT NULL , menu_status INTEGER DEFAULT 1 NOT NULL, menu_isdeleted INTEGER DEFAULT 0 NOT NULL,FOREIGN KEY (restaurant_id) REFERENCES restaurant_accounts (restaurant_id) ON UPDATE CASCADE ON DELETE CASCADE,FOREIGN KEY (category_id) REFERENCES menu_category (category_id) ON UPDATE CASCADE ON DELETE CASCADE);"
       let queryStatement : OpaquePointer? = try prepareStatement(query: query)
           guard sqlite3_step(queryStatement) == SQLITE_DONE else{
               
               throw SQLiteError.Step(message: errorMessage)
               
           }
           sqlite3_finalize(queryStatement)
        
   }
}
