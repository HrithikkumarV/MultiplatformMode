//
//  CreateRestaurantTables.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 02/03/22.
//

import Foundation
import SQLite3

extension DatabaseService {
    
    func initialiseCreateRestaurantsTables() throws{
        try createRestaurantAccountsTable()
        try createLocalPersistRestaurantId()
    }
    
    
    private func createRestaurantAccountsTable() throws{
        let query = "CREATE TABLE IF NOT EXISTS restaurant_accounts(restaurant_id INTEGER DEFAULT 0 PRIMARY KEY AUTOINCREMENT, restaurant_image BLOB NOT NULL, restaurant_name TEXT NOT NULL , restaurant_phone_number TEXT NOT NULL UNIQUE , resaurant_password TEXT NOT NULL, restaurant_cuisine TEXT NOT NULL, restaurant_description TEXT NOT NULL,restaurant_door_number_and_building_name_and_building_number TEXT NOT NULL, restaurant_street_name TEXT NOT NULL, restaurant_locality TEXT NOT NULL,restaurant_landmark TEXT NOT NULL, restaurant_city TEXT NOT NULL, restaurant_state TEXT NOT NULL,restaurant_country TEXT NOT NULL , restaurant_pincode TEXT NOT NULL,franchisor_id INTEGER, restaurant_rating_out_of_5 REAL DEFAULT 0.0 NOT NULL,restaurant_total_number_of_ratings INTEGER DEFAULT 0 NOT NULL ,restaurant_isavailable INTEGER DEFAULT 1 NOT NULL, restaurant_opens_next_at TEXT DEFAULT '' NOT NULL ,restaurant_food_packaging_charges INTEGER DEFAULT 0 NOT NULL,restaurant_joined_at TEXT NOT NULL , restaurant_account_status INTEGER DEFAULT 1 NOT NULL);"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
        guard sqlite3_step(queryStatement) == SQLITE_DONE else{
                throw SQLiteError.Step(message: errorMessage)
           
            }
        sqlite3_finalize(queryStatement)
    }
    
    private func createLocalPersistRestaurantId() throws{
        let query = "CREATE TABLE IF NOT EXISTS local_persist_restaurant_Id(local_id INTEGER DEFAULT 0 PRIMARY KEY , restaurant_id INTEGER NOT NULL);"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
            guard sqlite3_step(queryStatement) == SQLITE_DONE else{
                throw SQLiteError.Step(message: errorMessage)
            }
        
            sqlite3_finalize(queryStatement)
        
    }
}
