//
//  OrderDetails.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 14/03/22.
//

import Foundation

struct OrderDetails {
    
    var orderModel : OrderModel = OrderModel()
    var orderStatus : OrderStatus = .Pending
     var instructionToRestaurant : String = ""
     var restaurantName : String = ""
     var orderTotal : Int = 0
    var paymentMode : PaymentMode  = .COD
     var starRating : Int = 0
     var feedback : String = ""
  
}

struct OrderModel{
     var orderId : String = ""
     var restaurantId: Int = 0
     var userId: Int = 0
     var paymentId : String = ""
     var dateAndTime : String = ""
     var userAddressId : String  = ""
   
}

enum OrderStatus : String {
    case Pending
    case Preparing
    case Ready
    case Delivered
    case Cancelled
    case Declined
}
