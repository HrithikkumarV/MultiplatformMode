//
//  ZResponsiveSplitItem.swift
//  TestStoryboard
//
//  Created by Srihari M S on 19/05/17.
//  Copyright © 2017 Srihari M S. All rights reserved.
//

import Cocoa

typealias SplitMode = (modeName: String, minimumWidth: CGFloat, maximumWidth: CGFloat)

class ZResponsiveSplitItem: NSSplitViewItem
{
    var modeSetcallback: ((_: (modeName: String, minimumWidth: CGFloat, maximumWidth: CGFloat), _ itemIdentifier: String?) -> ())?
    
    var itemIdentifier: String?
    
    var isSideBarItem: Bool = false
    
    var dontSetModes = false
    
    private var prevFrame = NSRect.zero
    
    typealias ModeName = String
    var modecallbacks: [ModeName: SimpleClosure]! = [ModeName: SimpleClosure]()
    
    var viewModes: [(modeName: String, minimumWidth: CGFloat, maximumWidth: CGFloat)] = [(modeName: String, minimumWidth: CGFloat, maximumWidth: CGFloat)]()
    {
        didSet
        {
            viewModes.sort{ $0.minimumWidth < $1.minimumWidth }
            nextModeMin = nil
            currentModeIndex = nil
            currentMode = nil
            setMode()
        }
    }
    
    private(set) var currentMode: (modeName: String, minimumWidth: CGFloat, maximumWidth: CGFloat)?
    
    private var nextModeMin: CGFloat?
    private var currentModeIndex: Int?
    
    var isHiddenItem: Bool
    {
        return (minimumThickness == 0 && maximumThickness == 0) || view.isHiddenOrHasHiddenAncestor
    }
    
    var view = NSView()
    
    override var viewController: NSViewController {
        didSet {
            super.viewController = viewController
            self.view = viewController.view
        }
    }
    
    override init()
    {
        super.init()
        viewController.view.postsFrameChangedNotifications = true
        NotificationCenter.default.addObserver(self, selector: #selector(frameChanged), name: NSView.frameDidChangeNotification, object: nil)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func setMode(_ modeName: String)
    {
        if let index = viewModes.firstIndex(where: { $0.modeName == modeName })
        {
            setMode(viewModes[index], index: index)
        }
    }
    
    func hide()
    {
        minimumThickness = 0
        maximumThickness = 0
    }
    
    func getMode(for width: CGFloat) -> (modeName: String, minimumWidth: CGFloat, maximumWidth: CGFloat)?
    {
        for mode in viewModes.reversed()
        {
            if mode.minimumWidth <= width
            {
                return mode
            }
        }
        return viewModes.first
    }
    
    //MARK: - Private Functions
    
    @objc private func frameChanged(_ notification: Notification)
    {
        if !viewController.isViewLoaded {
            return
        }
        if self.view.frame.equalTo(self.prevFrame) {
            return
        }
        self.prevFrame = self.view.frame
        
        guard (notification.object as? NSView) === view else
        {
            return
        }
        
        setMode()
    }
    
    private func setMode(_ mode: (modeName: String, minimumWidth: CGFloat, maximumWidth: CGFloat), index: Int)
    {
        if minimumThickness > mode.maximumWidth && mode.maximumWidth != NSSplitViewItem.unspecifiedDimension
        {
            minimumThickness = mode.minimumWidth - 1
            maximumThickness = mode.maximumWidth
        }
        else
        {
            maximumThickness = mode.maximumWidth
            minimumThickness = mode.minimumWidth - 1
        }
        
        currentMode = mode
        modeSetcallback?(mode, itemIdentifier)
        currentModeIndex = index
        currentMode = mode
        if viewModes.count > index + 1
        {
            nextModeMin = viewModes[index + 1].minimumWidth
        }
        else
        {
            nextModeMin = nil
        }
    }
    
    private func setMode()
    {
        guard viewModes.count != 0 else
        {
            return
        }
        
        if isHiddenItem || dontSetModes
        {
            return
        }
        
        if let nextMin = nextModeMin, view.frame.width < nextMin, view.frame.width > minimumThickness
        {
            return
        }
        
        if nextModeMin == nil && currentMode != nil && view.frame.width > minimumThickness
        {
            return
        }
        
        if let currentIndex = currentModeIndex
        {
            
            if view.frame.width <= minimumThickness
            {
                for i in (viewModes.startIndex..<currentIndex).reversed()
                {
                    if viewModes[i].minimumWidth <= view.frame.width
                    {
                        setMode(viewModes[i], index: i)
                        return
                    }
                }
            }
            
            if view.frame.width >= maximumThickness
            {
                for i in (currentIndex..<viewModes.endIndex).reversed()
                {
                    if viewModes[i].minimumWidth <= view.frame.width
                    {
                        setMode(viewModes[i], index: i)
                        return
                    }
                }
            }
        }
        
        for (index, mode) in viewModes.enumerated().reversed()
        {
            if mode.minimumWidth <= view.frame.width
            {
                setMode(mode, index: index)
                return
            }
        }
        setMode(viewModes.first!, index: 0)
    }
}

class ZResponsiveSplitView: NSSplitView {
    
    var dividerBgColor: NSColor = NSColor.clear
    
//    override var dividerColor: NSColor {
//        return dividerBgColor
//    }
    
    override func drawDivider(in rect: NSRect) {
        dividerBgColor.set()
        rect.fill()
    }
}
