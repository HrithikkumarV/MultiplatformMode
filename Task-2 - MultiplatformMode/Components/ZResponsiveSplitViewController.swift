//
//  ZResponsiveSplitViewController.swift
//  TestStoryboard
//
//  Created by Srihari M S on 19/05/17.
//  Copyright © 2017 Srihari M S. All rights reserved.
//

import Cocoa
//import VTComponents

typealias SimpleClosure = () -> ()

protocol ZResponsiveSplitViewDelegate: NSSplitViewDelegate {
    func didExpandCollapseSideBarView(_ isSideBarCollapsed: Bool)
}

class ZResponsiveSplitViewController: NSSplitViewController
{
    //Most of the NSSplitViewDelegate fucntions cannot be overriden an are used by NSSplitViewController. See below for the functions which can be used.
    weak var delegate: ZResponsiveSplitViewDelegate?
    
    var modeSetCallback: ((_: (modeName: String, minimumWidth: CGFloat)) -> ())?
    
    public var shouldHideDividerAtFirstIndex = false
    
    //This has to be set before the view is loaded, otherwise it is ignored. Changing the super class' splitView raises an exception
    var customSplitView: NSSplitView?
    
    var setsSubItemMode = true
    
    var shouldGivecallback: () -> Bool = {true}
    
    var viewModes: [(modeName: String, minimumWidth: CGFloat)] = [(modeName: String, minimumWidth: CGFloat)]()
    {
        didSet
        {
            viewModes.sort{ $0.minimumWidth < $1.minimumWidth }
            nextModeMin = nil
            currentModeIndex = nil
            currentMode = nil
            setMode()
        }
    }
    
    private(set) var currentMode: (modeName: String, minimumWidth: CGFloat)?
    
    private var nextModeMin: CGFloat?
    
    private var currentModeIndex: Int?
    
    private var observer: NSKeyValueObservation?
    
    var minimumThickness: CGFloat?
    {
        return currentMode?.minimumWidth
    }
    
    //MARK: - View Life Cycle Functions
    override func viewDidLoad()
    {
        print("view did load")
        NotificationCenter.default.addObserver(self, selector: #selector(frameChanged), name: NSView.frameDidChangeNotification, object: nil)
//        if let customSplitView = customSplitView
//        {
//            splitView = customSplitView
//        }
        
        var sideViewSplitViewItem : ZResponsiveSplitItem?
        
        for item in (splitViewItems as! [ZResponsiveSplitItem]){
            if(item.isSideBarItem){
                sideViewSplitViewItem = item
                break
            }
        }

        observer = sideViewSplitViewItem?.observe(\.isCollapsed, options: [.initial, .new]) { [weak self]splitViewItem, _ in
            self?.delegate?.didExpandCollapseSideBarView(splitViewItem.isCollapsed)
            print("Sidebar collapsed state changed to: \(splitViewItem.isCollapsed)")
         }
        
        super.viewDidLoad()
    }
    
    deinit {
        view.removeFromSuperview()
        NotificationCenter.default.removeObserver(self)
    }
    
    func addSplitViewItem(_ splitViewItem: NSSplitViewItem, shouldMaintainSize: Bool = false/*, min: CGFloat?, max: CGFloat?*/)
    {
        //        let currentSize = view.frame.size
        //        if shouldMaintainSize
        //        {
        //            splitViewItem.minimumThickness = currentSize.width
        //            splitViewItem.maximumThickness = currentSize.width
        //        }
        super.addSplitViewItem(splitViewItem)
        //        if shouldMaintainSize
        //        {
        //            DLog("OldSize: \(view.frame.size), new size = \(currentSize)")
        //            view.setFrameSize(currentSize)
        //        }
    }
    
    func setMode(_ mode: (modeName: String, minimumWidth: CGFloat))
    {
        if let index = viewModes.firstIndex(where: { $0.modeName == mode.modeName && $0.minimumWidth == mode.minimumWidth})
        {
            setMode(mode, index: index)
        }
    }
    
    typealias ModeName = String
    typealias ItemIdentifier = String
    //Nil mode name implies that the item is hidden
    func setItemModes(modesForItems: [ItemIdentifier: ModeName?], afterAnimation: SimpleClosure? = nil)
    {
        for (itemIdentifier, mode) in modesForItems
        {
            if let item = splitViewItems.first(where: { ($0 as! ZResponsiveSplitItem).itemIdentifier == itemIdentifier }) as? ZResponsiveSplitItem
                
            {
                guard let nnMode = mode else
                {
                    
                    item.view.window!.setAnchorAttribute(.leading, for: .horizontal)
                    //                    dlog("Hiding: \(itemIdentifier)")
                    NSAnimationContext.runAnimationGroup({_ in item.animator().isCollapsed = true}, completionHandler:
                        {   item.minimumThickness = 0
                            item.maximumThickness = 0
                            afterAnimation?()})
                    continue
                }
                if item.isHiddenItem
                {
                    //                    dlog("Un-Hiding: \(itemIdentifier)")
                    item.maximumThickness = -1
                    NSAnimationContext.runAnimationGroup({_ in item.animator().isCollapsed = false}, completionHandler: afterAnimation)
                }
                else
                {
                    item.setMode(nnMode)
                    afterAnimation?()
                }
            }
        }
    }
    
    // If you want to sonething to either of the items after animation, which may affect the animation like
    // removing one of the item, do it in the closure in the afterAnimation parameter
    
    func replace(item: ItemIdentifier, with otherItem: ItemIdentifier, afterAnimation: SimpleClosure? = nil)
    {
        guard let itemToHave = splitViewItems.first(where: { ($0 as! ZResponsiveSplitItem).itemIdentifier == otherItem }) as? ZResponsiveSplitItem, let itemToRemove = splitViewItems.first(where: { ($0 as! ZResponsiveSplitItem).itemIdentifier == item }) as? ZResponsiveSplitItem else
        {
            return
        }
        
        
        itemToHave.dontSetModes = true
        itemToRemove.dontSetModes = true
        
        shouldGivecallback = {false}
        //        dlog("Mode setting stopped")
        
        let width = itemToRemove.view.frame.width
        
        //        dlog("Fixing width: \(width) for \(itemToHave.itemIdentifier!)")
        
        let constaint = itemToHave.view.widthAnchor.constraint(equalToConstant: width)
        
        itemToHave.minimumThickness = 0
        itemToHave.maximumThickness = -1
        
        itemToRemove.minimumThickness = 0
        
        constaint.isActive = true
        
        var tempConstraints = [(view: ZResponsiveSplitItem, constraint: NSLayoutConstraint)]()
        
        for thisSubview in splitViewItems
        {
            let thisItem = thisSubview as! ZResponsiveSplitItem
            if thisItem.itemIdentifier != item && thisItem.itemIdentifier != otherItem && !thisItem.isHiddenItem
            {
                let constraint = thisItem.view.widthAnchor.constraint(equalToConstant: thisItem.view.frame.width)
                NSLayoutConstraint.activate([constraint])
                thisItem.dontSetModes = true
                //                dlog("Dont set mode for: \(thisItem.itemIdentifier!) and width is \(constraint)")
                tempConstraints.append((view: thisItem, constraint: constraint))
            }
        }
        
        NSAnimationContext.runAnimationGroup(
            {_ in
                
                itemToHave.animator().isCollapsed = false
                itemToRemove.animator().isCollapsed = true
        }, completionHandler:
            {
                itemToHave.view.removeConstraint(constaint)
                for (item, constraint) in tempConstraints
                {
                    item.dontSetModes = false
                    item.view.removeConstraint(constraint)
                    //                dlog("Can set mode for: \(item.itemIdentifier!)")
                }
                itemToHave.dontSetModes = false
                itemToRemove.dontSetModes = false
                
                self.shouldGivecallback = {true}
                
                afterAnimation?()})
    }
    
    
    
    override func splitView(_ splitView: NSSplitView, shouldHideDividerAt dividerIndex: Int) -> Bool
    {
        if splitViewItems.count > dividerIndex && dividerIndex > 0
        {
            //            dlog("Returning: \(splitViewItems[dividerIndex].viewController.view.isHidden) for index: \(dividerIndex)")
            return splitViewItems[dividerIndex].isCollapsed
        }//[dividerIndex]
        //        dlog("Returning: false for index: \(dividerIndex)")
        return shouldHideDividerAtFirstIndex
    }
    
    //    override func shou
    //MARK: - Private Functions
    
    @objc private func frameChanged(_ notification: Notification)
    {
        guard (notification.object as? NSView) === view else
        {
            return
        }
        //dlog("Frame : \(view.frame.size)")
        setMode()
    }
    
    private func setMode(_ mode: (modeName: String, minimumWidth: CGFloat), index: Int)
    {
        if setsSubItemMode
        {
            for thisSplitViewItem in splitViewItems
            {
                (thisSplitViewItem as! ZResponsiveSplitItem).setMode(mode.modeName)
            }
        }
        
        currentMode = mode
        currentModeIndex = index
        currentMode = mode
        if viewModes.count > index + 1
        {
            nextModeMin = viewModes[index + 1].minimumWidth
        }
        print(mode)
        modeSetCallback?(mode)
    }
    
    private func setMode()
    {
        if setsSubItemMode == false
        {
            var mode = "No Mode"
            if let acutalMode = currentMode?.modeName {
                mode = acutalMode
            }
            print("Width: \(view.frame.width), mode: \(mode)")
        }
        
        if !isViewLoaded || !shouldGivecallback()
        {
            return
        }
        
        guard viewModes.count != 0 else
        {
            return
        }
        
        if let nextMin = nextModeMin, let currentMinimumTickness = minimumThickness, view.frame.width < nextMin, view.frame.width > currentMinimumTickness
        {
            return
        }
        
        if let currentMinimumTickness = minimumThickness, view.frame.width <= currentMinimumTickness, let currentIndex = currentModeIndex
        {
            for i in (viewModes.startIndex..<currentIndex).reversed()
            {
                if viewModes[i].minimumWidth <= view.frame.width
                {
                    setMode(viewModes[i], index: i)
                    return
                }
            }
        }
        
        for (index, mode) in viewModes.enumerated().reversed()
        {
            if mode.minimumWidth <= view.frame.width
            {
                setMode(mode, index: index)
                return
            }
        }
        setMode(viewModes.first!)
    }
    
    //MARK: - NSSplitViewDelegate Functions
    
    override func splitView(_ splitView: NSSplitView, canCollapseSubview subview: NSView) -> Bool
    {
        if let canCollapse = delegate?.splitView?(splitView, canCollapseSubview: subview) {
            return canCollapse
        }
        return super.splitView(splitView, canCollapseSubview: subview)
//        return delegate?.splitView?(splitView, canCollapseSubview: subview) ??  super.splitView(splitView, canCollapseSubview: subview)
    }
    
    override func splitView(_ splitView: NSSplitView, shouldCollapseSubview subview: NSView, forDoubleClickOnDividerAt dividerIndex: Int) -> Bool
    {
        return delegate?.splitView!(splitView, shouldCollapseSubview: subview, forDoubleClickOnDividerAt: dividerIndex) ?? super.splitView(splitView, shouldCollapseSubview: subview, forDoubleClickOnDividerAt: dividerIndex)
        
           
    }
    
    override func splitViewWillResizeSubviews(_ notification: Notification)
    {
        delegate?.splitViewWillResizeSubviews?(notification)
    }
    
    override func splitViewDidResizeSubviews(_ notification: Notification)
    {
        delegate?.splitViewDidResizeSubviews?(notification)
    }
}

